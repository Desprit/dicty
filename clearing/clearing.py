import re


def words_clearing(words):
	'''Clear text from numbers, prepositions, and non-letter symbols'''

	temp_list = []

	for i in words:

		i = re.sub('\n', ' ', i)
		i = re.sub('\d+', '', i)
		i = re.sub(r'\W+', ' ', i).lower()
		a = i.split(' ')
		for l in a:
			#--- Avoiding prepositions ---#
			if len(l) > 3:
				temp_list.append(l)
				
	return temp_list