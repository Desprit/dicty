# -*- coding: utf-8 -*-
from yandex_translate import YandexTranslate
import os
import time
import goslate
import re

#--- assigning Yandex.API-key ---#
translate = YandexTranslate('trnsl.1.1.20140703T103621Z.35ab6e76f4269775.1c9ff6b59da1fc58ac2f9d3847ea66b3ece09b2d')

gs = goslate.Goslate()
t = []

def translating(lemmatized_words, dictionary):
	'''Translate words using dictionary'''

	for i in lemmatized_words:
		translation = dictionary.get(i)
		if translation == None:
			tr = gs.translate(i, 'ru')
			t.append(tr)
		else:
			t.append(re.sub('\n', '', translation))

	return t

def translating_Yandex(lemmatized_words):
	'''Translate words using Yandex.API'''

	for i in lemmatized_words:
		translation = translate.translate(i, 'ru')['text']
		for a in translation:
			t.append(a)

	return t

def translating_backup(lemmatized_words):
	pass
	'''Translate words using Yandex.API'''

	l = len(lemmatized_words)
	n = (l - (l % number_of_threads)) / number_of_threads
	children = []

	for thread_number in range(number_of_threads):
		pid = os.fork()
		if pid != 0:
			'''print('Process {0} spawned' .format(pid))'''
			children.append(pid)
		else:
			for i in lemmatized_words[thread_number*n:thread_number*n+n]:
				print thread_number, i
				translation = translate.translate(i, 'ru')['text']
				for a in translation:
					t.append(a)
			os._exit(0)
		
	for z, child in enumerate(children):
		os.waitpid(child, 0)	

	print t
	return t