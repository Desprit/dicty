from operator import itemgetter


def complexity_rank(lemmatized_words, word_ranking):
	'''Calculate text complexity'''

	total_rank 		 = 0

	for i in lemmatized_words:
		try:
			total_rank += int(word_ranking.get(i + '\r'))
		except:
			continue

	avarage_rank = total_rank / len(lemmatized_words)

	return avarage_rank


def word_ranking(translated_words, word_ranking):
	'''To every word assign it's rank'''

	final_words_list = []

	for key, value in translated_words.iteritems():
		temp 	= []
		temp[:] = []
		try:
			rank 	= int(word_ranking.get(key + '\r'))
		except:
			rank = 0
		temp 	= [key, value, rank]
		final_words_list.append(temp)

	sorted_ranked_words = list(reversed(sorted(final_words_list, key=itemgetter(2))))
	return sorted_ranked_words


def text_statistics(ranked_words):
	'''Collect text statistics'''

	text_statistics = []
	

	easy_words = 0
	for i in ranked_words:
		if i[2] <= 4000 and i[2] != 0:
			easy_words += 1

	difficult_words = 0
	for i in ranked_words:
		if i[2] > 4000:
			difficult_words += 1

	uncategorized_words = 0
	for i in ranked_words:
		if i[2] == 0:
			uncategorized_words += 1

	text_statistics.append(easy_words)
	text_statistics.append(difficult_words)
	text_statistics.append(uncategorized_words)

	return text_statistics