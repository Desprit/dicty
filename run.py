# -*- coding: utf-8 -*-
from clearing import clearing
from normalization import normalization
from translation import translation
from statistics import statistics
import sys
import json


#--- Import raw text ---#
file_name = 'little_prince'
opener 	  = open('texts/' + file_name + '.txt').read()
writer    = open('texts/analized/' + file_name + '_analized.txt', 'w')
words     = opener.split(' ')

word_ranking = open('eng_freq_55000_dict.txt')
word_ranking = json.load(word_ranking)

dictionary	 = open('translation/muller_dict.txt')
dictionary	 = json.load(dictionary)

cleared_words    = clearing.words_clearing(words)
pos_tagged_words = normalization.pos_tagging(cleared_words)
lemmatized_words = normalization.lemmatizing(pos_tagged_words)
complexity_rank  = statistics.complexity_rank(lemmatized_words, word_ranking)
translated_words = dict(zip(lemmatized_words, translation.translating(lemmatized_words, dictionary)))

ranked_words 	 = statistics.word_ranking(translated_words, word_ranking)
text_statistics  = statistics.text_statistics(ranked_words)


#--- Stage 5 of 8: Writing to file ---#
#print 'Doing stage 8 of 8: Writing to file...'
writer.write('Total number of words in raw text: {0}{1}' .format(str(len(cleared_words)), 	'\n'	 ))
writer.write('Total number of unique words: {0}{1}' 	 .format(str(len(lemmatized_words)),'\n'	 ))
writer.write('Text complexity: {0}{1}{2}' 				 .format(str(complexity_rank), 		'\n','\n'))
writer.write('Easy words: {0}{1}' 						 .format(str(text_statistics[0]), 	'\n'	 ))
writer.write('Difficult words: {0}{1}' 					 .format(str(text_statistics[1]), 	'\n'	 ))
writer.write('Uncategorized words: {0}{1}{2}' 			 .format(str(text_statistics[2]), 	'\n','\n'))

for i in ranked_words:

	writer.write('{0:10}{1:15}{2:15}' .format(str(i[2]), str(i[0]), str(i[1].encode('utf-8'))))
	writer.write('\n')

writer.close()

