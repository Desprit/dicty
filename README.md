Script was created and tested on Ubuntu Linux with Python 2.7.5 installed.
-----------------------------------------------------------------------------------------------------------


This script analysis text for it's complexity, prepares the list of unique words and it's translations.

At this moment the script only works for **English** texts and translates it into **Russian**.

To make this scrip work there are some steps to be done:
-----------------------------------------------------------------------------------

Modules to be installed:
=================

- [nltk] with it's dictionaries *- to analize words*
- [goslate] *- to translate words by using google translation API*

Step-by-step instruction:
=================
* Make sure all modules are installed;
* Create a folder for the script and extract files there;
* Create a folder 'texts' and folder 'analized' inside of 'texts';
* Put the text you want to analize inside 'texts' folder;
* Open **configure.py** file and setup 'file_name' var.

**Now just:**

```cd your_project_folder```

```python run.py```

**Speed**:
it usually takes about 30 sec for a book as 'The Little Prince' (8685 raw words)



Example of output:
--------------------------
* Total number of words in **raw text**: 8685
* Total number of **unique words**: 1593
* Text **complexity**: 4546
- Easy words: 1081 
- Difficult words: 456 
- Uncategorized words: 56


81351 - baobab - баобаб (дерево)  
76956 - eaten - съеденный



Things to be implemented later:
---------------------------------------------
* **Multithreading** for faster translation.
* Improve offline dictionary's quality



**Please leave your feedback:**

a.bondarev.it@gmail.com

[xlrd]:http://www.python-excel.org/
[xlwt]:http://www.python-excel.org/
[selenium]:https://pypi.python.org/pypi/selenium
[mechanize]:https://pypi.python.org/pypi/mechanize/
[beautiful soup]:https://pypi.python.org/pypi/beautifulsoup4/4.3.2
[nltk]: http://www.nltk.org/
[goslate]: https://pypi.python.org/pypi/goslate