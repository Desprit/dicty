# -*- coding: utf-8 -
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import word_tokenize, pos_tag
import nltk
from nltk.stem import PorterStemmer

#--- Lemmatizing and translation module setup ---#
lmtzr = WordNetLemmatizer()


def pos_tagging(cleared_words):
	'''Define part of speech by using NLTK pos_tag() method'''

	cleared_words 	 = list(set(cleared_words))
	pos_tagged_words = []

	for i in cleared_words:
		tag = nltk.pos_tag(i.split())
		pos_tagged_words.append(tag)

	return pos_tagged_words



def lemmatizing(pos_tagged_words):
	'''Define word's lemma by using NLTK WordNetLemmatizer() method'''

	lemmatized_words = []

	for i in pos_tagged_words:
		lemmatized_word = ''
		if   i[0][1] == 'JJS':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='a')
		elif i[0][1] == 'JJ':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='a')
		elif i[0][1] == 'JJR':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='a')
		elif i[0][1] == 'VB':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='v')
		elif i[0][1] == 'VBD':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='v')
		elif i[0][1] == 'VBG':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='v')
		elif i[0][1] == 'VBN':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='v')
		elif i[0][1] == 'VBP':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='v')
		elif i[0][1] == 'VBZ':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='v')
		elif i[0][1] == 'NN':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='n')
		elif i[0][1] == 'NNS':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='n')
		elif i[0][1] == 'NNP':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='n')
		elif i[0][1] == 'NNPS':
			lemmatized_word = lmtzr.lemmatize(i[0][0], pos='n')
		else:
			lemmatized_word = ''.join([lmtzr.lemmatize(a,b[0].lower()) if b[0].lower() in ['a','n','v'] else lmtzr.lemmatize(a) for a,b in pos_tag(i[0][0])])
		
		lemmatized_words.append(lemmatized_word.encode('utf-8'))

	lemmatized_words = list(set(lemmatized_words))

	return lemmatized_words